import java.util.Scanner;

public class M08App {
    private static String nombre = "";
    private static String apellido = "";

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int opcion;
        do {
            try{
                System.out.println("Menú:");
                System.out.println("1 - Añadir nombre");
                System.out.println("2 - Añadir apellido");
                System.out.println("3 - Mostrar nombre completo");
                System.out.println("4 - Salir");
                System.out.print("Selecciona una opción: ");

                opcion = scanner.nextInt();

            switch (opcion) {
                case 1:
                    System.out.print("Introduce el nombre: ");
                    nombre = scanner.next();
                    break;
                case 2:
                    System.out.print("Introduce el apellido: ");
                    apellido = scanner.next();
                    break;
                case 3:
                    if (!nombre.isEmpty() && !apellido.isEmpty()) {
                        System.out.println("Nombre completo: " + nombre + " " + apellido);
                    } else {
                        System.out.println("Debes añadir nombre y apellido primero.");
                    }
                    break;
                case 4:
                    System.out.println("Saliendo del programa.");
                    break;
                default:
                    System.out.println("Opción no válida. Inténtalo de nuevo.");
                switch (opcion) {
                    case 1:
                        System.out.print("Introduce el nombre: ");
                        nombre = scanner.next();
                        break;
                    case 2:
                        System.out.print("Introduce el apellido: ");
                        apellido = scanner.next();
                        break;
                    case 3:
                        if (!nombre.isEmpty() && !apellido.isEmpty()) {
                            System.out.println("Nombre completo: " + nombre + " " + apellido);
                        } else {
                            System.out.println("Debes añadir nombre y apellido primero.");
                        }
                        break;
                    case 4:
                        System.out.println("Saliendo del programa.");
                        break;
                    default:
                        System.out.println("Opción no válida. Inténtalo de nuevo.");
                }
            }catch(InputMismatchException e){
                System.out.println("Opción no válida. Inténtalo de nuevo.");
                scanner.nextLine();  // Consumir la entrada incorrecta
                opcion = 0;
            }
        } while (opcion != 4);

        	// Espera antes de mostrar el menú nuevamente
        	try {
            	    Thread.sleep(1000);
        	} catch (InterruptedException e) {
            	    e.printStackTrace();
        	}
	    }
        } while (opcion != 4);
        scanner.close();
    }
}

